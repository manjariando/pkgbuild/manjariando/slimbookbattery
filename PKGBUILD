# Maintainer: Mark Wagie <mark dot wagie at tutanota dot com>
# Contributor: tioguda <guda.flavio@gmail.com>

pkgname=slimbookbattery
_pkgver=3.97
pkgver=${_pkgver}beta
pkgrel=4
pkgdesc="A battery optimization application for portable devices"
arch=('x86_64')
url="https://slimbook.es"
license=('CC BY-NC-ND 3.0')
options=('!strip')
makedepends=('imagemagick')
install=${pkgname}.install
source=("https://launchpad.net/~slimbook/+archive/ubuntu/slimbook/+files/${pkgname}_${pkgver}_all.deb"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}preferences.metainfo.xml")
sha512sums=('78180850612726d87fddd3030292f4f1eb1344cdd228b536ecf4cae87b86a63bfb7eae67237d52061d85c07768754402881a026da0ef2419177c83549eae4181'
            '3e57d1df8c0f4088195a7c1be29446922b0a8ae544dc717b90d1e3f2e827399f52888ce0dd80d57718cd7d65efd8f21c26e9dbc1966e6a68581a5937d1279175'
            '9f78d3f1274ec237d40e189e3dd8f99b94139669ae2de4a5b7755c367e4dec0ebceff2c5b09267f8947d9e932bf9408863772355f75aa0f8b344b2e199c04cb5')

package(){
    depends=('python-gobject' 'python-cairo' 'python-pillow' 'python-dbus' 'libappindicator-gtk3'
            'gobject-introspection' 'libnotify' 'tlp-rdw' 'cron' 'dmidecode' 'xorg-xdpyinfo')
    optdepends=('nvidia-prime: for hybrid graphics switching'
                'gnome-shell-extension-appindicator: for tray icon on GNOME')

    # Extract package data
    bsdtar xf data.tar.xz -C "$pkgdir"

    install -d "$pkgdir/usr/lib/systemd/system"
    mv "${pkgdir}/etc/systemd/system/suspend-sedation.service" "${pkgdir}/usr/lib/systemd/system"
    rm -rf "${pkgdir}/etc/systemd"

    # Appstream
    install -d "${pkgdir}"/usr/share/{applications,licenses/${pkgname},metainfo}
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"
    install -Dm644 "${srcdir}/com.${pkgname}preferences.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}preferences.metainfo.xml"
    mv "${pkgdir}/usr/share/doc/${pkgname}/copyright" "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
    mv "${pkgdir}/usr/share/applications/${pkgname}.desktop" "${pkgdir}/usr/share/applications/com.${pkgname}.desktop"
    mv "${pkgdir}/usr/share/applications/${pkgname}preferences.desktop" "${pkgdir}/usr/share/applications/com.${pkgname}preferences.desktop"

    # Fix and install desktop icons
    for size in 22 24 32 48 64 128 256 512; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${pkgdir}/usr/share/pixmaps/${pkgname}.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${pkgname}.png"
        convert "${pkgdir}/usr/share/pixmaps/${pkgname}preferences.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${pkgname}preferences.png"
    done

    # Archify folder permissions
    cd ${pkgdir}
    for d in $(find ${pkgdir}/etc/sudoers.d/ -type d); do
        chmod 750 $d
    done
}
